//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let posOdd=[]
    let negEven=[]
     let a = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    for(let i=0; i<a.length; i++){
        if(a[i]>0 && a[i]%2!==0){
            posOdd.push(a[i])
        }
        else if(a[i]<0 && a[i]%2===0){
            negEven.push(a[i])
        }
    }
output+='Positive odd:'+posOdd+'\n';
output+='Negative even:'+negEven+'\n';
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    let a=0
    let b=0
    let c=0
    let d=0
    let e=0
    let f=0
    for(let i=0; i<60000;i++){
        let s=Math.floor((Math.random() * 6) + 1)
        if(s==1){
            a++
        }
        else if(s==2){
            b++
        }
        else if(s==3){
            c++
        }
        else if(s==4){
            d++
        }
        else if(s==5){
            e++
        }
        else if(s==6){
            f++
        }
    }
    output+='Frequency of die rolls'+'\n'
    output+='1: '+a+'\n'
    output+='2: '+b+'\n'
    output+='3: '+c+'\n'
    output+='4: '+d+'\n'
    output+='5: '+e+'\n'
    output+='6: '+f+'\n'
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let w=[0,0,0,0,0,0,0]
   for(let i=0; i<60000;i++){
      let die=Math.floor((Math.random() * 6) + 1)
    w[die]++
   }
       output+='Frequency of die rolls'+'\n'
    output+='1: '+w[1]+'\n'
    output+='2: '+w[2]+'\n'
    output+='3: '+w[3]+'\n'
    output+='4: '+w[4]+'\n'
    output+='5: '+w[5]+'\n'
    output+='6: '+w[6]+'\n'
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    let w=[0,0,0,0,0,0,0]
   for(let i=0; i<60000;i++){
      let die=Math.floor((Math.random() * 6) + 1)
    w[die]++
   }
    let expected=10000
    let d=expected*0.01
var dieRolls = {
    Total:60000,
Frequencies: {
1:w[1],
2:w[2],
3:w[3],
4:w[4],
5:w[5],
6:w[6],
},
Exceptions: ""
}
        output+="Frequency of dice rolls"+"\n"+"Total rolls: 60000"+"\n"
        output+="Frequencies:"+"\n"
    for (let prop in dieRolls.Frequencies){
        
        output+=prop+": "+dieRolls.Frequencies[prop]+"\n"
    }
    //output+="Exceptions:"+dieRolls.Exceptions
for (let i=1;i<=6;i++){
    if(Math.abs(dieRolls.Frequencies[i]-expected)>=100){
        output+="Exceptions:"+i+"\n"
    }
    }
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
        let person = {
        name: "Jane",
        income: 127050
}
if(person.income>0 && person.income<=18200){
    'Nile'
}
else if(person.income>18200 && person.income<=37000){
    tax=(person.income-18200)*0.19
}
else if(person.income>37000 && person.income<=90000){
    tax=(person.income-37000)*0.325+3572
}
else if(person.income>90000 && person.income<=180000){
    tax=(person.income-90000)*0.37+20797
}
else if(person.income>180000){
    tax=(person.income-180000)*0.45+54097
}
output+='Jane’s income is: $'+person.income+', '+'and her tax owed is: $'+tax.toFixed(2)
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}