//task 1
var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};

function objectToHTML(obj){
    let string="";
    for(let prop in obj){
        string+=prop+": "+obj[prop]+"<br/>"
    }
    return string
}
let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerHTML = objectToHTML(testObj) //this line will fill the above element with your output.