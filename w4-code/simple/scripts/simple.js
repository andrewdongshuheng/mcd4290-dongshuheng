function num1AndNum2AndNum3() {
    let answer=""
    let num1 = document.getElementById("number1");
    let num2 = document.getElementById("number2");
    let num3 = document.getElementById("number3");
    let answerRef = document.getElementById("answer");
    let oddOrEven=document.getElementById("oddOrEven");
    answer=Number(num1.value)+Number(num2.value)+Number(num3.value);
    if(answer>=0){
        answerRef.className = "positive";
    }
    else{
        answerRef.className = "negative";
    }
    
    if(answer%2==0){
        oddOrEven.innerHTML="(even)"
        oddOrEven.className="even"
    }
    else{
        oddOrEven.innerHTML="(odd)"
        oddOrEven.className="odd"
    }
    answerRef.innerHTML = answer;
}
function doIt() {
    let clickButton = document.getElementById("doIt");
    num1AndNum2AndNum3()
}